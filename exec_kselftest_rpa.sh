#Cloning kselftest
CWD="$(pwd)"
echo -e "`git clone 'https://github.com/torvalds/linux.git'`\n"

cd linux
echo -e "`make -C tools/testing/selftests`"  #To build the tests


#To install selftests in default location
cd tools/testing/selftests
echo -e "`./kselftest_install.sh`"  #

cd $CWD

#LTP integration to RPA

#Step 1: Download the source code from following link (or from git): 

echo -e "`git clone 'https://github.com/robotframework/robotframework.git'`\n" #Installing RPA

#Step 2: If you already have Python 2.7 with pip installed, you can simply run:
echo -e "`pip install robotframework`"

#Step 4: After that you can install the framework with:
cd robotframework
echo -e "`sudo python setup.py install`"

cd $CWD

#Creating .robot file


#Running Kselftest through RPA
echo -e "`robot u540-kselftest.robot`" #it creates log.html, output.xml and report.html

#merging two output files
#Assuming , we are merging output1.xml and output2.xml  for comparing the outputs

#echo -e "`rebot -R --xunit newfile.xml output1.xml output2.xml`" # command to merege the files

#<newfile.xml> ,log.html and report.html are new files created after merging
#output1.xml -->old status and output2.xml --> new status

#rebot --outputdir OUTPUT -o main -l main_log -r main_r --name Test_execution output1.xml output2.xml
