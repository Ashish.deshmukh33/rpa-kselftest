*** Setting ***
Library    OperatingSystem
Library    BuiltIn
Library    String
Library    Process



*** Variables ***

${KSPATH}   ${CURDIR}/linux/tools/testing/selftests/
## 3 space and at last '/'##


*** Test Cases ***

###############################  android  ######################################################


################################  breakpoints ##################################################


###############################  capabilities ###################################################
test_execve
	[Tags]   capabilities	
	${result}=	Run	cd ${KSPATH}/capabilities && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/capabilities && ./test_execve
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}




###############################  cgroup ###################################################
test_freezer
	[Tags]   cgroup	
	${result}=	Run	cd ${KSPATH}/cgroup && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	${result}=	Run	cd ${KSPATH}/cgroup && ./test_freezer
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


###############################  clone3 ###################################################

clone3
	[Tags]	clone3
	${result}=	Run	cd ${KSPATH}/clone3 && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/clone3 && ./clone3
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

clone3_clear_sighand
	[Tags]	clone3
	${result}=	Run	cd ${KSPATH}/clone3 && ./clone3_clear_sighand
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

clone3_set_tid
	[Tags]	clone3
	${result}=	Run	cd ${KSPATH}/clone3 && ./clone3_set_tid
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



###############################  cpufreq ###################################################

###############################  cpu-hotplug ###################################################

###############################  drivers ###################################################


###############################  efivarfs ###################################################


###############################  exec ###################################################
execveat
	[Tags]   exec	
	${result}=	Run	cd ${KSPATH}/exec && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/exec && ./execveat
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


recursion-depth
	[Tags]   exec
	${result}=	Run	cd ${KSPATH}/exec && ./recursion-depth
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



###############################  filesystems [**]###################################################
devpts_pts
	[Tags]   filesystems	
	${result}=	Run	cd ${KSPATH}/filesystems && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

	${result}=	Run	cd ${KSPATH}/filesystems && ./devpts_pts
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}






###############################  firmware ###################################################
###############################  ftrace [stuk]###################################################
###############################  futex ###################################################
futex-run.sh
	[Tags]   futex
	${result}=	Run	cd ${KSPATH}/futex && ./run.sh
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



###############################  gpio ###################################################

###############################  intel_pstate ###################################################

###############################  ipc [**]###################################################


###############################  ir ###################################################

###############################  kcmp [**]###################################################
kcmp_test
	[Tags]   kcmp	
	${result}=	Run	cd ${KSPATH}/kcmp && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

	${result}=	Run	cd ${KSPATH}/kcmp && ./kcmp_test
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}







###############################  lib #######################################################


###############################  livepatch ###################################################


###############################  lkdtm ###################################################



###############################  membarrier ###################################################
membarrier_test_single_thread
	[Tags]   membarrier	
	${result}=	Run	cd ${KSPATH}/membarrier && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/membarrier && ./membarrier_test_single_thread
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


membarrier_test_multi_thread
	[Tags]   membarrier
	${result}=	Run	cd ${KSPATH}/membarrier && ./membarrier_test_multi_thread
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


###############################  memfd ###################################################
memfd_test
	[Tags]   memfd	
	${result}=	Run	cd ${KSPATH}/memfd && make
	
	${result}=	Run	cd ${KSPATH}/memfd && ./memfd_test
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    Permission denied
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    failed
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    Aborted
	${error_10}=	Get Lines Containing String   ${result}    Operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'failed' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'Aborted' in '''${result}'''     FAIL	${error_09}
	...   ELSE IF	'Operation not permitted' in '''${result}'''     FAIL	${error_10}

fuse_test
	[Tags]   memfd
	${result}=	Run	cd ${KSPATH}/memfd && ./fuse_test
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}
fuse_mnt
	[Tags]   memfd
	${result}=	Run	cd ${KSPATH}/memfd && ./fuse_mnt
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



run_fuse_test.sh
	[Tags]   memfd
	${result}=	Run	cd ${KSPATH}/memfd && ./run_fuse_test.sh
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

run_hugetlbfs_test.sh
	[Tags]   memfd
	${result}=	Run	cd ${KSPATH}/memfd && ./run_hugetlbfs_test.sh
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}




###############################  memory-hotplug ###################################################

###############################  mount ###################################################

run_tests.sh
	[Tags]   mount	
	${result}=	Run	cd ${KSPATH}/mount && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/mount && ./run_tests.sh
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


###############################  mqueue [**]###################################################


mq_perf_tests  ## take time
	[Tags]   mqueue
	${result}=	Run	cd ${KSPATH}/mqueue && ./mq_perf_tests
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

###############################  net ###################################################
reuseport_dualstack
	[Tags]   net
	${result}=	Run	cd ${KSPATH}/net && make
	Log   ${result}


		${result}=	Run	cd ${KSPATH}/net && ./reuseport_dualstack
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


reuseaddr_conflict
	[Tags]   net
		${result}=	Run	cd ${KSPATH}/net && ./reuseaddr_conflict
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

tls
	[Tags]   net
		${result}=	Run	cd ${KSPATH}/net && ./tls
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



#################################################################################################

udpgso.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./udpgso.sh
#fail is part of test
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

ip_defrag.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./ip_defrag.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


msg_zerocopy.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./msg_zerocopy.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    Error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'Error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


reuseport_addr_any.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./reuseport_addr_any.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    Error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'Error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


test_vxlan_fdb_changelink.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./test_vxlan_fdb_changelink.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    Cannot find device
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'Cannot find device' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


ipv6_flowlabel.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./ipv6_flowlabel.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



traceroute.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./traceroute.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    Error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'Error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


##################################################################################################

fin_ack_lat.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./fin_ack_lat.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    Error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'Error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


route_localnet.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./route_localnet.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    Error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'Error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

reuseaddr_ports_exhausted.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./reuseaddr_ports_exhausted.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    Error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'Error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

txtimestamp.sh
	[Tags]    net
	${result}=	Run	cd ${KSPATH}/net && ./txtimestamp.sh
	Log To Console    ${result}
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    Error
	${error_07}=	Get Lines Containing String   ${result}    command not found
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'Error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'command not found' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



###############################  net/forwarding ###################################################
###############################  net/mptcp ###################################################

###############################  netfilter ###################################################


###############################  nsfs ###################################################
owner
	[Tags]   nsfs	
	${result}=	Run	cd ${KSPATH}/nsfs && make
	Log   ${result}

	${result}=	Run	cd ${KSPATH}/nsfs && ./owner			
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

pidns
	[Tags]   nsfs
	${result}=	Run	cd ${KSPATH}/nsfs && ./pidns
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

###############################  pidfd ###################################################
pidfd_fdinfo_test
	[Tags]   pidfd	
	${result}=	Run	cd ${KSPATH}/pidfd && make
	Log   ${result}

	${result}=	Run	cd ${KSPATH}/pidfd && ./pidfd_fdinfo_test
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


pidfd_test
	[Tags]   pidfd
	${result}=	Run	cd ${KSPATH}/pidfd && ./pidfd_test
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}




###############################  proc ###################################################
fd-001-lookup
	[Tags]   proc	
	${result}=	Run	cd ${KSPATH}/proc && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/proc && ./fd-001-lookup
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


fd-002-posix-eq
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./fd-002-posix-eq
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

fd-003-kthread
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./fd-003-kthread
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


proc-loadavg-001
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-loadavg-001
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


proc-pid-vm
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-pid-vm
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}
proc-self-map-files-001
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-self-map-files-001
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


proc-self-map-files-002
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-self-map-files-002
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

proc-self-syscall
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-self-syscall
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

proc-self-wchan
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-self-wchan
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

proc-uptime-001
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-uptime-001
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


proc-uptime-002
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./proc-uptime-002
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

self
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./self
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

setns-dcache
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./setns-dcache
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


setns-sysvipc
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./setns-sysvipc
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}
thread-self
	[Tags]   proc
	${result}=	Run	cd ${KSPATH}/proc && ./thread-self
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

###############################  pstore [**]###################################################


###############################  ptrace ###################################################
get_syscall_info
	[Tags]   ptrace	
	${result}=	Run	cd ${KSPATH}/ptrace && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

	${result}=	Run	cd ${KSPATH}/ptrace && ./get_syscall_info
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


peeksiginfo
	[Tags]   ptrace
	${result}=	Run	cd ${KSPATH}/ptrace && ./peeksiginfo
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

vmaccess
	[Tags]   ptrace
	${result}=	Run	cd ${KSPATH}/ptrace && ./vmaccess
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


###############################  rtc [take time on x86] ###################################################
###############################  seccomp ###################################################


###############################  sigaltstack ###################################################
sas
	[Tags]   sigaltstack	
	${result}=	Run	cd ${KSPATH}/sigaltstack && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

	${result}=	Run	cd ${KSPATH}/sigaltstack && ./sas
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

###############################  size ###################################################


###############################  splice ###################################################
default_file_splice_read.sh
	[Tags]   splice	
	${result}=	Run	cd ${KSPATH}/splice && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/splice && ./default_file_splice_read.sh
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}



###############################  static_keys ###################################################

###############################  sync ###################################################

###############################  sysctl ###################################################

###############################  timens ###################################################

###############################  timers ###################################################
posix_timers
	[Tags]   timers	
	${result}=	Run	cd ${KSPATH}/timers && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


	${result}=	Run	cd ${KSPATH}/timers && ./posix_timers
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

nanosleep
	[Tags]   timers
	${result}=	Run	cd ${KSPATH}/timers && ./nanosleep
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


nsleep-lat
	[Tags]   timers
	${result}=	Run	cd ${KSPATH}/timers && ./nsleep-lat
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

set-timer-lat
	[Tags]   timers
	${result}=	Run	cd ${KSPATH}/timers && ./set-timer-lat
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


mqueue-lat
	[Tags]   timers
	${result}=	Run	cd ${KSPATH}/timers && ./mqueue-lat
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


inconsistency-check
	[Tags]   timers
	${result}=	Run	cd ${KSPATH}/timers && ./inconsistency-check
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

threadtest
	[Tags]   timers
	${result}=	Run	cd ${KSPATH}/timers && ./threadtest
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


###############################  tmpfs ###################################################
bug-link-o-tmpfile
	[Tags]   tmpfs	
	${result}=	Run	cd ${KSPATH}/tmpfs && make
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}

	${result}=	Run	cd ${KSPATH}/tmpfs && ./bug-link-o-tmpfile
	Log   ${result}
	${error_01}=	Get Lines Containing String   ${result}    not ok
	${error_02}=	Get Lines Containing String   ${result}    Bail out
	${error_03}=	Get Lines Containing String   ${result}    not set
	${error_04}=	Get Lines Containing String   ${result}    FAIL
	${error_05}=	Get Lines Containing String   ${result}    FAILED
	${error_06}=	Get Lines Containing String   ${result}    error
	${error_07}=	Get Lines Containing String   ${result}    not been executed
	${error_08}=	Get Lines Containing String   ${result}    skipping test
	${error_09}=	Get Lines Containing String   ${result}    operation not permitted

	Run Keyword If 	'not ok' in '''${result}'''     FAIL	${error_01}
	...   ELSE IF  	'Bail out' in '''${result}'''     FAIL	${error_02}
	...   ELSE IF 	'not set' in '''${result}'''     FAIL	${error_03}
	...   ELSE IF 	'FAIL' in '''${result}'''     FAIL	${error_04}
	...   ELSE IF 	'FAILED' in '''${result}'''     FAIL	${error_05}
	...   ELSE IF 	'error' in '''${result}'''     FAIL	${error_06}
	...   ELSE IF 	'not been executed' in '''${result}'''     FAIL	${error_07}
	...   ELSE IF 	'skipping test' in '''${result}'''     FAIL	${error_08}
	...   ELSE IF	'operation not permitted' in '''${result}'''     FAIL	${error_09}


###############################  tpm2 ###################################################


###############################  user ###################################################


###############################  vm ###################################################


###############################  x86 ###################################################



###############################  zram ###################################################

######################################################################################

